export interface GeolocationResponseInterface {
    results: Array<{
        formatted_address: string;
        geometry: {
            location: {
                lat: number;
                lng: number;
            }
        }
    }>;
}
