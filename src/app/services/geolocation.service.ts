import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {GeolocationResponseInterface} from '../models/geolocationResponse.interface';

@Injectable()
export class GeolocationService {

    constructor(protected httpClient: HttpClient) {

    }

    // Google Geolocation API Key
    private key = 'AIzaSyBrXhwslNLDRCvWl4X_WjS5HznvMixhBQs';

    /**
     * Does an HTTP Request to Google to get the latitude and longitude of an address
     * @param address
     * @returns {Promise<GeolocationResponseInterface>}
     */
    public getAddress(address): Promise<GeolocationResponseInterface> {
        return new Promise<GeolocationResponseInterface>((resolve, reject) => {
            // Always add Nederland to the search query
            // to avoid receiving results from other countries
            address = address + ' Nederland';
            this.httpClient.get<GeolocationResponseInterface>('https://maps.googleapis.com/maps/api/geocode/json?' +
            'key=' + this.key + '&address=' + address)
                .subscribe(data => {
                    resolve(data);
                }, err => {
                    reject(err);
                });
        });
    }

    /**
     * Calculates the distance
     *
     * @return {number}
     * @param latitude_from
     * @param longitude_from
     * @param latitude_to
     * @param longitude_to
     */
    public getDistanceTo(latitude_from: number, longitude_from: number, latitude_to: number, longitude_to: number): number {
        return ( 6371 * Math.acos(Math.cos(this.radians(latitude_from))
            * Math.cos(this.radians(latitude_to))
            * Math.cos(
                this.radians(longitude_to) - this.radians(longitude_from))
            + Math.sin(this.radians(latitude_from))
            * Math.sin(this.radians(latitude_to))) ) * 1000;
    }

    /**
     * Calculates radians
     *
     * @param degrees
     * @return {number}
     */
    protected radians(degrees: number) {
        return degrees * Math.PI / 180;
    }
}
