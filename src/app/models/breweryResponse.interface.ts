import {Brewery} from './brewery';

export interface BreweryResponseInterface {
    breweries: Brewery[];
}
