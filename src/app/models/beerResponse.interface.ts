import {Beer} from './beer';

export interface BeerResponseInterface {
    beers: Beer[];
}
