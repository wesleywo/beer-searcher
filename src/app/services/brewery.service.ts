import {Injectable} from '@angular/core';
import {Brewery} from '../models/brewery';
import {HttpClient} from '@angular/common/http';
import {BreweryResponseInterface} from '../models/breweryResponse.interface';
import {Beer} from '../models/beer';
import {BeerResponseInterface} from '../models/beerResponse.interface';
import {GeolocationService} from './geolocation.service';

@Injectable()
export class BreweryService {

    // Use local variables for caching
    protected _breweries: Brewery[] = [];
    protected _beers: Beer[] = [];

    constructor(protected httpClient: HttpClient,
                protected geolocationService: GeolocationService) {

    }

    /**
     * Get list of breweries
     * @returns {Brewery[]}
     */
    get breweries(): Brewery[] {
        return this._breweries;
    }

    /**
     * Fetch breweries from server
     */
    public update(): Promise<void> {
        return new Promise<void>((resolve, reject) => {
            // When both breweries and beers have been imported
            // continue to map them
            let i = 0;
            const func = () => {
                // Map beers to breweries
                this.mapBeers();
                // Use Google to get the lat, lng from breweries
                this.getAddressFromBreweries().catch((err) => {
                    console.error(err);
                });
                // Sort breweries by name by default
                this._breweries.sort((a, b) => {
                    return a.name < b.name ? -1 : 1;
                });
                resolve();
            };

            // Get the breweries
            this.httpClient.get<BreweryResponseInterface>('./assets/data/breweries.json').subscribe(data => {
                this._breweries = data.breweries;
                if (++i === 2) {
                    func();
                }
            }, err => {
                reject(err);
            });

            // Get the beers
            this.httpClient.get<BeerResponseInterface>('./assets/data/beers.json').subscribe(data => {
                this._beers = data.beers;
                if (++i === 2) {
                    func();
                }
            }, err => {
                reject(err);
            });
        });

    }

    /**
     * Map beers to breweries
     */
    protected mapBeers() {
        for (const brewery of this._breweries) {
            brewery.beers = [];
            for (const beer of this._beers) {
                if (beer.brewery === brewery.name) {
                    brewery.beers.push(beer);
                }
            }
        }
    }

    /**
     * Use Google to get the lat,lng from breweries
     * @returns {Promise<void>}
     */
    public getAddressFromBreweries(): Promise<void> {
        return new Promise<void>((resolve, reject) => {
            for (const brewery of this._breweries) {
                this.geolocationService.getAddress(brewery.address + ' ' + brewery.zipcode + ' ' + brewery.city).then((data) => {
                    brewery.lat = data.results[0].geometry.location.lat;
                    brewery.lng = data.results[0].geometry.location.lng;
                    resolve();
                }, err => {
                    reject(err);
                });
            }
        });
    }
}
