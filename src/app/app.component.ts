import {AfterViewInit, Component} from '@angular/core';
import {BreweryService} from './services/brewery.service';
import {Brewery} from './models/brewery';
import {GeolocationService} from './services/geolocation.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements AfterViewInit {
    // Title of the app and site
    title = 'Beer Searcher';
    // Search query for filtering
    protected searchQuery = '';
    // Search query for getting breweries location based
    protected geolocationQuery = '';
    // Array if more than one result is returned from Google
    protected geolocationArray = [];

    constructor(protected breweryService: BreweryService,
                protected geolocationService: GeolocationService) {

    }

    ngAfterViewInit() {
        // Automatically receive all breweries when loading
        this.update();
    }

    /**
     * Get breweries from service
     * @returns {Brewery[]}
     */
    get breweries(): Brewery[] {
        // If nothing is filled into the search bar, return everything
        if (this.searchQuery.length === 0) {
            return this.breweryService.breweries;
        }
        // Otherwise, filter breweries based on what's filled in
        const searchQuery = this.searchQuery.toLowerCase();
        return this.breweryService.breweries.filter(brewery => {
            return brewery.name.toLowerCase().indexOf(searchQuery) >= 0
                || brewery.address.toLowerCase().indexOf(searchQuery) >= 0
                || brewery.city.toLowerCase().indexOf(searchQuery) >= 0;
        });
    }

    /**
     * Update brewery service
     */
    update() {
        this.breweryService.update().catch((err) => {
            console.error(err);
        });
    }

    /**
     * Get the distances from addresses
     */
    public getDistances() {
        // When nothing is found or filled in, use func
        const func = () => {
            this.geolocationArray = [];
            this.breweryService.breweries.sort((a, b) => {
                return a.name < b.name ? -1 : 1;
            });
        };

        // If nothing is filled into the location search bar, return everything ordered by name
        if (this.geolocationQuery.length === 0) {
            func();
        } else {
            this.geolocationService.getAddress(this.geolocationQuery).then((data) => {
                // If Google found more than one result, pass it onto the array
                if (data.results.length > 1) {
                    this.geolocationArray = data.results;
                } else if (data.results.length === 1) {
                    // If Google found exactly one result,
                    // use that one and return results
                    this.geolocationArray = [];
                    const lat = data.results[0].geometry.location.lat;
                    const lng = data.results[0].geometry.location.lng;
                    this.sortByDistance(lat, lng);
                } else {
                    // If nothing is found, return empty array
                    func();
                }
            }, err => {
                // Log any errors
                console.error(err);
            });
        }
    }

    /**
     * Order distance from addresses
     * @param lat
     * @param lng
     */
    protected sortByDistance(lat, lng) {
        // Give every brewery object distance
        for (const brewery of this.breweryService.breweries) {
            if (brewery.lat) {
                brewery.distance = this.geolocationService.getDistanceTo(lat, lng, brewery.lat, brewery.lng);
            }
        }
        // Sort breweries by distance
        this.breweryService.breweries.sort((a, b) => {
            return a.distance < b.distance ? -1 : 1;
        });
    }
}
