import {Beer} from './beer';

export class Brewery {
    name: string;
    address: string;
    zipcode: string;
    city: string;
    open: string[];
    beers?: Beer[];
    lat?: number;
    lng?: number;
    distance?: number;
}
