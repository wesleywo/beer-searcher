export class Beer {
    brewery: string;
    name: string;
    style: string;
    volume: number;
    alcohol: number;
    keg: string;
}
